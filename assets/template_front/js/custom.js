var tpj = jQuery;
tpj.noConflict();

tpj(document).ready(function() {

    tpj('.form-send').submit(function (e) {
        e.preventDefault();
        tpj('.container-loading').hide().removeClass('hide').fadeIn('fast');
        tpj.ajax({
            url: tpj(this).attr('action'),
            type: tpj(this).attr('method'),
            data: tpj(this).serialize(),
            success: function (response) {
                tpj('.container-loading').fadeOut('fast').addClass('hide');
                var data = JSON.parse(response);
                tpj('div').removeClass('has-error');
                tpj('.help-block').remove();

                if(data.status === 'success') {
                    Swal.fire({
                        title: data.title,
                        text: data.message,
                        icon: 'success',
                        confirmButtonText: 'Baik'
                    }).then(function (result) {
                        if (result.value) {
                            window.location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: data.title,
                        text: data.message,
                        icon: 'error',
                        confirmButtonText: 'Baik'
                    });

                    tpj.each(data.errors  , function(field, desc) {
                        if(desc) {
                            tpj('[name="'+field+'"]').parent('div').addClass('has-error');
                            tpj('[name="'+field+'"]').after('<span class="help-block">'+desc+'</span>');
                        }
                    });
                }
            }
        });

        return false;
    });
});

var tpj=jQuery;
var revapi31;
tpj(document).ready(function() {
    if(tpj("#rev_slider_679_1").revolution == undefined){
        revslider_showDoubleJqueryError("#rev_slider_679_1");
    }else{
        revapi31 = tpj("#rev_slider_679_1").show().revolution({
            sliderType:"standard",
            jsFileLocation:"include/rs-plugin/js/",
            sliderLayout:"fullwidth",
            dottedOverlay:"none",
            delay:16000,
            hideThumbs:200,
            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,
            navigation: {
                keyboardNavigation: "on",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                touch: {
                    touchenabled: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                },
                arrows: {
                    style: "hades",
                    enable: false,
                    hide_onmobile: false,
                    hide_onleave: false,
                    tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 10,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 10,
                        v_offset: 0
                    }
                },
            },
            responsiveLevels:[1140,1024,778,480],
            visibilityLevels:[1140,1024,778,480],
            gridwidth:[1140,1024,778,480],
            gridheight:[700,768,960,720],
            lazyType:"none",
            shadow:0,
            spinner:"off",
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            fullScreenAutoWidth:"off",
            fullScreenAlignForce:"off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "0px",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
                simplifyAll:"off",
                nextSlideOnWindowFocus:"off",
                disableFocusListener:false,
            }
        });
    }

    revapi31.bind("revolution.slide.onloaded",function (e) {
        setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
    });

    revapi31.bind("revolution.slide.onchange",function (e,data) {
        SEMICOLON.slider.revolutionSliderMenu();
    });
});	/*ready*/
