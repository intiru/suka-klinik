module.exports = function (grunt) {

    var jsFiles = [
        'assets/template_front/js/jquery.js',
        'assets/template_front/js/plugins.js',
        'assets/template_front/js/sweetalert.js',
        'assets/template_front/include/rs-plugin/js/jquery.themepunch.tools.min.js',
        'assets/template_front/include/rs-plugin/js/jquery.themepunch.revolution.min.js',
        'assets/template_front/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js',
        'assets/template_front/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js',
        'assets/template_front/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js',
        'assets/template_front/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js',
        'assets/template_front/js/custom.js',
        'assets/template_front/js/functions.js'
    ];
    var cssFiles = [
        'assets/template_front/css/google-fonts.css',
        'assets/template_front/css/bootstrap.css',
        'assets/template_front/style.css',
        'assets/template_front/css/dark.css',
        'assets/template_front/css/font-icons.css',
        'assets/template_front/css/animate.css',
        'assets/template_front/css/magnific-popup.css',
        'assets/template_front/css/responsive.css',
        'assets/template_front/include/rs-plugin/css/settings.css',
        // 'assets/template_front/include/rs-plugin/css/layers.css',
        // 'assets/template_front/include/rs-plugin/css/navigation.css',
        'assets/template_front/css/custom.css',
        'assets/template_front/css/loading.css'
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'js/',
        cssDistDir: 'assets/template_front/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};