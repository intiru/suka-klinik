<div class="section parallax nobottommargin nobottomborder"
     style="background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>');"
     data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix dark">
        <div class="heading-block center">
            <h1><?php echo $page->title ?></h1>
            <span><?php echo $page->title_sub ?></span>
        </div>

    </div>

</div>
<section id="content">

    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row">
                <?php foreach ($doctor_list as $row) { ?>
                    <div class="col-lg-3 col-md-6 bottommargin">
                        <div class="team">
                            <div class="team-image">
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                     alt="<?php echo $row->thumbnail_alt ?>" title="<?php echo $row->thumbnail_alt ?>">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4><?php echo $row->title ?></h4><span><?php echo $row->description  ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>