<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="postcontent nobottommargin clearfix">
                <div class="single-post nobottommargin">
                    <div class="entry clearfix">
                        <div class="entry-title">
                            <h1><?php echo $page->title ?></h1>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><i class="icon-calendar3"></i> <?php echo $this->main->date_view($page->created_at) ?></li>
                            <li><a href="#"><i class="icon-user"></i> Admin Suka Klinik</a></li>
                            <li><i class="icon-folder-open"></i> <a href="<?php echo $this->main->permalink(array('blog', $page->category_title)) ?>"><?php echo $page->category_title ?></a></li>
                            <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                        </ul>
                        <div class="entry-content notopmargin">
                            <div class="entry-image alignleft">
                                <a href="<?php echo $this->main->image_preview_url($page->thumbnail) ?>" data-lightbox="image">
                                    <img src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>" alt="<?php echo $page->thumbnail_alt ?>">
                                </a>
                            </div>

                            <?php echo $page->description ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar nobottommargin col_last clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget clearfix">
                        <h4>Kategori Blog</h4>
                        <div class="tagcloud">
                            <?php foreach ($category as $row) { ?>
                                <a href="<?php echo $this->main->permalink(array('blog', $row->title)) ?>"><?php echo $row->title ?></a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="widget clearfix">
                        <div class="tabs nobottommargin clearfix" id="sidebar-tabs">
                            <ul class="tab-nav clearfix">
                                <li><a href="#tabs-1">Popular</a></li>
                                <li><a href="#tabs-2">Recent</a></li>
                            </ul>
                            <div class="tab-container">
                                <div class="tab-content clearfix" id="tabs-1">
                                    <div id="popular-post-list-sidebar">

                                        <?php foreach ($blog_popular as $row) {
                                            $link = $this->main->permalink(array('blog', $row->title)); ?>

                                            <div class="spost clearfix">
                                                <div class="entry-image">
                                                    <a href="<?php echo $link ?>" class="nobg">
                                                        <img class="rounded-circle"
                                                             src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                             alt="<?php echo $row->thumbnail_alt ?>">
                                                    </a>
                                                </div>
                                                <div class="entry-c">
                                                    <div class="entry-title">
                                                        <h4><a href="<?php echo $link ?>"><?php echo $row->title ?></a>
                                                        </h4>
                                                    </div>
                                                    <ul class="entry-meta">
                                                        <li>
                                                            <i class="icon-eye-open"></i> <?php echo number_format($row->views) ?>
                                                            Views
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="tab-content clearfix" id="tabs-2">
                                    <div id="recent-post-list-sidebar">

                                        <?php foreach ($blog_recent as $row) {
                                            $link = $this->main->permalink(array('blog', $row->title)); ?>

                                            <div class="spost clearfix">
                                                <div class="entry-image">
                                                    <a href="<?php echo $link ?>" class="nobg">
                                                        <img class="rounded-circle"
                                                             src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                             alt="<?php echo $row->thumbnail_alt ?>">
                                                    </a>
                                                </div>
                                                <div class="entry-c">
                                                    <div class="entry-title">
                                                        <h4><a href="<?php echo $link ?>"><?php echo $row->title ?></a>
                                                        </h4>
                                                    </div>
                                                    <ul class="entry-meta">
                                                        <li>
                                                            <?php echo $this->main->date_view($row->created_at) ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        <?php } ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>