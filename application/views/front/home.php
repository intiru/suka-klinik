<section id="slider" class="slider-element revslider-wrap clearfix">
    <div class="slider-parallax-inner">

        <div id="rev_slider_679_1_wrapper" class="rev_slider_wrapper fullwidth-container" style="padding:0px;">
            <div id="rev_slider_679_1" class="rev_slider fullwidthbanner" style="display:none;" data-version="5.1.4">
                <ul>
                    <?php foreach ($slider as $row) { ?>

                        <li class="dark" data-transition="fade" data-slotamount="0" data-masterspeed="0"
                            data-thumb="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                            data-delay="0" data-saveperformance="off" data-title="Responsive Design">
                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                 alt="<?php echo $row->thumbnail_alt ?>" data-bgposition="center bottom"
                                 data-bgpositionend="center top" data-kenburns="on" data-duration="10000"
                                 data-ease="Linear.easeNone" data-scalestart="0" data-scaleend="0"
                                 data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0"
                                 data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                            <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
                                 data-x="middle" data-hoffset="0"
                                 data-y="top" data-voffset="380"
                                 data-fontsize="['60','50','40','34']"
                                 data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                 data-speed="800"
                                 data-start="1200"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-endeasing="Power4.easeIn"
                                 style="z-index: 3; white-space: nowrap; line-height: 60px; text-align: center !important;"><?php echo $row->title ?></div>

                            <div class="tp-caption customin ltl tp-resizeme"
                                 data-x="middle" data-hoffset="0"
                                 data-y="top" data-voffset="500"
                                 data-transform_in="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
                                 data-speed="800"
                                 data-start="1550"
                                 data-easing="easeOutQuad"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-elementdelay="0.01"
                                 data-endelementdelay="0.1"
                                 data-endspeed="1000"
                                 data-endeasing="Power4.easeIn" style="z-index: 3;">
                                <a href="<?php echo site_url('kontak-kami') ?>" class="button button-xlarge tright">
                                    <span>Hubungi Kami</span> <i class="icon-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>

    </div>
</section>

<section id="content">
    <div class="content-wrap">
        <div class="container clearfix notopmargin">
            <div class="col_full heading-block center">
                <h1><?php echo $home_services_title->title ?></h1>
                <span>
                <p><?php echo $home_services_title->title_sub ?></p>
                    </span>
            </div>
            <div class="row">
                <?php foreach($home_services_list as $row) : ?>
                <div class="col-12 col-sm-6 col-md-3 col-lg-3 bottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                 alt="<?php echo $row->thumbnail_alt ?>" title="<?php echo $row->thumbnail_alt ?>">
                        </div>
                        <h3><?php echo $row->title ?></h3>
                        <?php echo $row->description ?>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>
        </div>

        <div class="clearfix"></div>

        <div class="border-top nobottommargin nobottompadding">
            <div class="row">
                <div class="col-12 text-center">
                    <h3>Telpon, WhatsApp atau Email Suka Klinik dengan menekan tombol di bawah : </h3>

                    <a href="<?php echo $whatsapp_link ?>" class="button button-xlarge tright"><i
                                class="icon-call"></i> Chat WhatsApp</a>
                    <a href="<?php echo $phone_hp_link ?>" class="button button-xlarge tright"><i
                                class="icon-call"></i> Telepon HP</a>
                    <a href="<?php echo $phone_office_link ?>" class="button button-xlarge tright"><i
                                class="icon-call"></i> Telepon Klinik</a>
                    <a href="<?php echo $email_link ?>" class="button button-xlarge tright"><i
                                class="icon-email"></i> Kirim Email</a>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-12">
                    <h3 align="center">Alamat Suka Klinik :</h3>
                    <a href="<?php echo $address_link ?>" target="_blank">
                        <p class="home-address"><?php echo $address ?></p>
                    </a>
                </div>
            </div>
        </div>

        <div class="section layanan-unggulan">

            <div class="container nobottommargin nopadding clearfix">

                <div class="heading-block center">
                    <h2><?php echo $home_services_top_title->title ?></h2>
                    <span><?php echo $home_services_top_title->title_sub ?></span>
                </div>

                <div class="row">

                    <?php foreach($home_services_top_list as $row) { ?>

                        <div class="col-12 col-sm-6 col-md-6 bottommargin">
                            <div class="feature-box fbox-plain fbox-small fbox-dark">
                                <div class="fbox-icon">
                                    <i class="icon-line-star"></i>
                                </div>
                                <h3><?php echo $row->title ?></h3>
                                <?php echo $row->description ?>
                            </div>
                        </div>

                    <?php } ?>

                </div>
            </div>

        </div>

    </div>


    <div class="container clearfix">
        <div class="col_one_fourth">
            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                <h4><?php echo $home_siapa->title ?></h4>
            </div>
            <?php echo $home_siapa->description ?>
        </div>
        <div class="col_one_fourth">
            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                <h4><?php echo $home_moto->title ?></h4>
            </div>
            <?php echo $home_moto->description ?>
        </div>

        <div class="col_one_fourth">
            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                <h4><?php echo $home_visi->title ?></h4>
            </div>
            <?php echo $home_visi->description ?>
        </div>

        <div class="col_one_fourth col_last">
            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                <h4><?php echo $home_misi->title ?></h4>
            </div>
            <?php echo $home_misi->description ?>
        </div>
    </div>

</section>

<?php if($promo_home) { ?>

<div class="modal-on-load" data-target="#myModal1"></div>

<div class="modal1 mfp-hide" id="myModal1">
    <div class="block divcenter modal-promo-home-block">

        <img src="<?php echo base_url('assets/template_front/images/promo-sign.png') ?>" class="modal-promo-home-sign">
        <div class="center modal-promo-home-wrapper">
            <img src="<?php echo $this->main->image_preview_url($promo_home->thumbnail) ?>" alt="<?php echo $promo_home->meta_title ?>">
        </div>
        <div class="section nomargin modal-promo-home-footer">
            <a href="#" class="button" onClick="$.magnificPopup.close();return false;">Tutup </a>
        </div>
    </div>
</div>

<?php } ?>