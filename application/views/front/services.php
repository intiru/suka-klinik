<div class="section parallax nobottommargin nobottomborder"
     style="background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>');"
     data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix dark">
        <div class="heading-block center">
            <h1><?php echo $page->title ?></h1>
            <span><?php echo $page->title_sub ?></span>
        </div>

    </div>

</div>
<section id="content">

    <div class="content-wrap">
        <div class="container clearfix">

            <div class="row">
                <?php foreach ($services_list as $row) : ?>
                    <div class="col-12 col-md-4 bottommargin-sm">
                        <div class="feature-box media-box">
                            <div class="fbox-media">
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                     alt="<?php echo $row->thumbnail_alt ?>" title="<?php echo $row->title ?>">
                            </div>
                            <div class="fbox-desc">
                                <h2><?php echo $row->title ?>
                                    <span class="subtitle">
                                    <?php echo $row->title_sub ?>
                                </span>
                                </h2>
                                <?php echo $row->description ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>

        </div>

        <div class="section parallax nobottommargin dark"
             style="background-image: url('<?php echo base_url() ?>assets/template_front/images/parallax/7.jpg'); padding: 100px 0;"
             data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
            <div class="heading-block center nobottomborder nobottommargin">
                <h2><?php echo $services_quotes ?></h2>
            </div>
        </div>

        <div class="section notopmargin footer-stick">
            <h2 class="uppercase center"><?php echo $testimonial_title ?></h2>
            <div class="container clearfix">
                <div id="oc-testi" class="owl-carousel testimonials-carousel carousel-widget" data-margin="20"
                     data-items-sm="1" data-items-md="2" data-items-xl="3">

                    <?php foreach ($testimonial_list as $row) { ?>
                        <div class="oc-item">
                            <div class="testimonial">
                                <div class="testi-content">
                                    <p><?php echo $row->description ?></p>
                                    <div class="testi-meta">
                                        <?php echo $row->title ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>

            <div class="center">
                <br />
                <a href="<?php echo site_url('testimonial') ?>" class="btn btn-success">Lihat Semua Testimonial</a>
            </div>
        </div>
    </div>
</section>