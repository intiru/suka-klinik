<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/template_front/css/suka-klinik.min.css">

    <link rel="icon" href="<?php echo base_url() ?>assets/template_front/images/favicon.png" type="image/png">
    <link href=”<?php echo base_url() ?>assets/template_front/images/favicon.png" rel="apple-touch-icon"/>

</head>

<body class="stretched">

<div id="wrapper" class="clearfix">
    <div id="top-bar">
        <div class="container clearfix">
            <div class="col_half nobottommargin">
                <div class="top-links">
                    <ul>
                        <li><a href="<?php echo $phone_office_link ?>">Telp : <?php echo $phone_office ?></a></li>
                        <li><a href="<?php echo $phone_hp_link ?>">HP : <?php echo $phone_hp ?></a></li>
                        <li><a href="<?php echo $whatsapp_link ?>">WA : <?php echo $whatsapp ?></a></li>
                        <li><a href="<?php echo $address_link ?>"><?php echo $address_short ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col_half fright col_last nobottommargin">
                <div id="top-social">
                    <ul>
                        <li><a href="<?php echo $facebook_link ?>" target="_blank" class="si-facebook"><span
                                        class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a>
                        </li>
                        <li><a href="<?php echo $instagram_link ?>" target="_blank" class="si-instagram"><span
                                        class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <header id="header" class="transparent-header semi-transparent" data-sticky-class="not-dark">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <div id="logo">
                    <a href="<?php echo site_url() ?>" class="standard-logo"
                       data-dark-logo="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"><img
                                src="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"
                                alt="Canvas Logo"></a>
                    <a href="<?php echo site_url() ?>" class="retina-logo"
                       data-dark-logo="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"><img
                                src="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"
                                alt="Canvas Logo"></a>
                </div>

                <nav id="primary-menu" class="dark">
                    <ul>
                        <li<?php echo $page->type == 'home' ? ' class="current"' : '' ?>><a
                                    href="<?php echo site_url() ?>">
                                <div>Beranda</div>
                            </a></li>
                        <li<?php echo $page->type == 'services' ? ' class="current"' : '' ?>>
                            <a href="<?php echo site_url('layanan') ?>">
                                <div>Layanan Kami</div>
                            </a>
                        </li>
                        <li<?php echo $page->type == 'doctor' ? ' class="current"' : '' ?>>
                            <a href="<?php echo site_url('profil-dokter') ?>">
                                <div>Profil Dokter</div>
                            </a>
                        </li>
                        <?php if($view_secret) { ?>
                        <li<?php echo $page->type == 'promo' ? ' class="current"' : '' ?>>
                            <a href="<?php echo site_url('promo') ?>">
                                <div>Promo</div>
                            </a>
                        </li>
                        <?php } ?>
                        <li<?php echo $page->type == 'gallery_photo' ? ' class="current"' : '' ?>><a
                                    href="<?php echo site_url('galeri-foto') ?>">
                                <div>Galeri</div>
                            </a>
                        </li>
                        <?php if($view_secret) { ?>
                        <li<?php echo $page->type == 'testimonial' ? ' class="current"' : '' ?>><a
                                    href="<?php echo site_url('testimonial') ?>">
                                <div>Testimonial</div>
                            </a>
                        </li>
                        <?php } ?>
                        <li<?php echo $page->type == 'about_us' ? ' class="current"' : '' ?>><a
                                    href="<?php echo site_url('tentang-kami') ?>">
                                <div>Tentang Kami</div>
                            </a>
                        </li>
                        <li<?php echo $page->type == 'contact_us' ? ' class="current"' : '' ?>><a
                                    href="<?php echo site_url('kontak-kami') ?>">
                                <div>Kontak Kami</div>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <?php echo $content ?>

    <footer id="footer" class="dark">
        <div class="container"
             style="background: url('<?php echo base_url() ?>assets/template_front/images/world-map.png') no-repeat center center; background-size: 100%;">
            <div class="footer-widgets-wrap clearfix">
                <div class="col_full">
                    <div class="widget clearfix center">
                        <img src="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png" alt=""
                             height="100">
                        <?php echo $footer_desc ?>
                        <address>
                            <strong>Alamat:</strong><br>
                            <a href="<?php echo $address_link ?>" target="_blank"><?php echo $address ?></a>
                        </address>
                        <abbr title="Phone Number">
                            <strong>HP / WhatsApp:</strong>
                        </abbr>
                        <a href="<?php echo $whatsapp_link ?>" target="_blank">
                            <?php echo $whatsapp ?>
                        </a><br>
                        <abbr title="Telepon">
                            <strong>Telepon:</strong>
                        </abbr>
                        <a href="<?php echo $phone_office_link ?>" target="_blank">
                            <?php echo $phone_office ?>
                        </a><br>
                        <abbr title="Email Address">
                            <strong>Email:</strong>
                        </abbr>
                        <a href="<?php echo $email_link ?>" target="_blank">
                            <?php echo $email ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="copyrights">
            <div class="container clearfix">
                <div class="col_one_fourth">
                    Suka Klinik © <?php echo date('Y') ?> <a href="https://www.intiru.com" target="_blank">INTIRU</a>
                    Developed
                </div>
                <div class="col_three_fourth col_last">
                    <div class="fright clearfix">
                        <div class="copyrights-menu copyright-links nobottommargin">
                            <a href="<?php echo site_url() ?>">Beranda</a>/
                            <a href="<?php echo site_url('layanan') ?>">Layanan Kami</a>/
                            <a href="<?php echo site_url('profil-dokter') ?>">Profil Dokter</a>/
                            <a href="<?php echo site_url('promo') ?>">Promo</a>/
                            <a href="<?php echo site_url('galeri-foto') ?>">Galeri</a>/
                            <a href="<?php echo site_url('blog') ?>">Blog</a>/
                            <a href="<?php echo site_url('testimonial') ?>">Testimonial</a>/
                            <a href="<?php echo site_url('tentang-kami') ?>">Tentang Kami</a>/
                            <a href="<?php echo site_url('kontak-kami') ?>">Kontak Kami</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class='container-loading hide'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>

<a href="<?php echo $whatsapp_link ?>" class="chat-whatsapp">
    <img src="<?php echo base_url() ?>assets/template_front/images/chat-whatsapp.png" alt="chat whatsapp asuransi">
</a>

<script type="text/javascript" src="<?php echo base_url() ?>js/suka-klinik.min.js"></script>

</body>
</html>