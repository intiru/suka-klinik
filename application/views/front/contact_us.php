<section id="page-title" class="page-title-parallax page-title-dark"
         style="padding: 120px 0; background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>'); background-size: cover; background-position: top;"
         data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px 0px;">
    <div class="container clearfix center">
        <h1>Kontak Kami</h1>
        <span>Everything you need to know about our Company</span>
    </div>
</section>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row bottommargin">
                <div class="col-lg-3 col-md-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-map-marker2"></i></a>
                        </div>
                        <h3>Alamat Suka Klinik<span class="subtitle"><a href="<?php echo $address_link ?>"
                                                                        target="_blank"><?php echo $address_short ?></a></span>
                        </h3>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-phone3"></i></a>
                        </div>
                        <h3>Telpon Suka Klinik<span class="subtitle"><a href="<?php echo $phone_office_link ?>"
                                                                        target="_blank"><?php echo $phone_office ?></a></span>
                        </h3>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-phone"></i></a>
                        </div>
                        <h3>WhatsApp Suka Klinik<span class="subtitle"><a href="<?php echo $whatsapp_link ?>"
                                                                          target="_blank"><?php echo $whatsapp ?></a></span>
                        </h3>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 bottommargin clearfix">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="#"><i class="icon-mail"></i></a>
                        </div>
                        <h3>Email Suka Klinik<span class="subtitle"><a href="<?php echo $email_link ?>"
                                                                       target="_blank"><?php echo $email ?></a></span>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col_half">
                <div class="fancy-title title-dotted-border">
                    <h3>Kirim Email ke Suka Klinik Dalung</h3>
                </div>

                <form class="nobottommargin form-send" action="<?php echo site_url('kontak-kami-send') ?>"
                      method="post">
                    <div class="col_one_third">
                        <label for="template-contactform-name">Nama <small>*</small></label>
                        <input type="text" name="name" value="" class="sm-form-control"/>
                    </div>
                    <div class="col_one_third">
                        <label for="template-contactform-email">Email <small>*</small></label>
                        <input type="text" name="email" value="" class="sm-form-control"/>
                    </div>
                    <div class="col_one_third col_last">
                        <label for="template-contactform-phone">Telepon/HP/WA</label>
                        <input type="text" name="phone" value="" class="sm-form-control"/>
                    </div>
                    <div class="clear"></div>
                    <div class="col_full">
                        <label for="template-contactform-subject">Judul Pesan<small>*</small></label>
                        <input type="text" name="subject" value="" class="sm-form-control"/>
                    </div>
                    <div class="clear"></div>
                    <div class="col_full">
                        <label for="template-contactform-message">Isi Pesan <small>*</small></label>
                        <textarea class="sm-form-control" name="message" rows="6" cols="30"></textarea>
                    </div>
                    <div class="col_full">
                        <label for="template-contactform-message">Kode Keamanan <small>*</small></label>
                        <br />
                        <?php echo $captcha ?>
                        <br />
                        <br />
                        <input type="text" name="captcha" value="" class="sm-form-control"/>
                    </div>
                    <div class="col_full">
                        <button name="submit" type="submit" value="Submit" class="button button-3d nomargin">Kirim
                            Pesan
                        </button>
                    </div>
                </form>

            </div>
            <div class="col_half col_last">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d64331.82211780835!2d115.16285741302084!3d-8.620581983395375!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x176d28a789eeb6d8!2sSUKA%20KLINIK%20%26%20DENTAL!5e0!3m2!1sid!2sid!4v1595884954485!5m2!1sid!2sid"
                        width="100%" height="80" frameborder="0" style="border:0;" allowfullscreen=""
                        aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
