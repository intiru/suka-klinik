<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <h2>Pasar Sukawati</h2>
                <h3>Supported By</h3>
                <img src="<?php echo base_url('assets/template_front/img/logo-bri-footer.png') ?>"
                     class="img-responsive" width="100%">
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 data-target="#collapse_1">Site Map</h3>
                <div class="collapse dont-collapse-sm links" id="collapse_1">
                    <ul>
                        <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                        <li><a href="<?php echo site_url('cara-pemesanan') ?>">Cara Pemesanan</a></li>
                        <li><a href="<?php echo site_url('kategori') ?>">Kategori</a></li>
                        <li><a href="<?php echo site_url('tekomendasi-toko') ?>">Rekomendasi Toko</a></li>
                        <li><a href="<?php echo site_url('lokasi') ?>">Lokasi</a></li>
                        <li><a href="<?php echo site_url('pengaduan') ?>">Pengaduan</a></li>
                        <li><a href="<?php echo site_url('toko') ?>">List Toko</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 data-target="#collapse_1">Kategori</h3>
                <div class="collapse dont-collapse-sm links" id="collapse_1">
                    <ul>
                        <?php foreach($category as $row) { ?>
                            <li><a href="<?php echo $this->main->permalink(array($row->title)) ?>"><?php echo $row->title.' ('.$row->shop_total.')' ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 data-target="#collapse_2">Hubungi Kami</h3>
                <div class="collapse dont-collapse-sm links" id="collapse_2">
                    <p>Lokasi : Jalan Raya Sukawati</p>
                    <p>Telepon : Kantor Teras BRI Sukawati Telp : <a href="telp:0361290132">0361 290132</a></p>
                    <p>Email : <a href="mailto:info@pasarsukawati.com">info@pasarsukawati.com</a></p>
                </div>
            </div>
        </div>

        <br/>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <p align="center">© Copyright 2020, <strong>Pasar Sukawati</strong>. All Rights Reserved.
                    BRI Kantor Cabang Gianyar</p>
                <p align="center">Development by <a href="http://intiru.com" target="_blank" class="dev">INTIRU -
                        Website Development</a></p>
            </div>
        </div>
    </div>
</footer>