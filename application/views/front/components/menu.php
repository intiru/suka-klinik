<div class="container">
    <div id="logo">
        <a href="<?php echo site_url() ?>">
            PASAR SUKAWATI
        </a>
    </div>

    <a href="#0" class="open_close">
        <i class="icon_menu"></i><span>Menu</span>
    </a>
    <nav class="main-menu">
        <div id="header_menu">
            <a href="#0" class="open_close">
                <i class="icon_close"></i><span>Menu</span>
            </a>
            <a href="<?php echo site_url() ?>">Pasar Sukawati</a>
        </div>
        <ul>
            <li<?php echo $page->type == 'home' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url() ?>">Beranda</a>
            </li>
            <li<?php echo $page->type == 'order' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('cara-pemesanan') ?>">Cara Pemesanan</a>
            </li>
            <li<?php echo $page->type == 'category' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('kategori') ?>">Kategori</a>
            </li>
            <li<?php echo $page->type == 'recommend' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('rekomendasi-toko') ?>">Rekomendasi Toko</a>
            </li>
            <li<?php echo $page->type == 'location' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('lokasi') ?>">Lokasi</a>
            </li>
            <li<?php echo $page->type == 'report' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('pengaduan') ?>">Pengaduan</a>
            </li>
            <li<?php echo $page->type == 'list_shop' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('toko') ?>">List Toko</a>
            </li>
        </ul>
    </nav>
</div>