<main class="bg_color">
    <div class="collapse" id="collapseMap">
        <div id="map" class="map"></div>
    </div>
    <div class="wrap-lokasi">
        <div class="container margin_60_40">
            <div class="page_header">
                <div class="breadcrumbs text-center">
                    <ul>
                        <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                        <li><?php echo $page->title ?></li>
                    </ul>
                </div>
            </div>
            <div class="main_title center">
                <span><em></em></span>
                <h1><?php echo $page->title ?></h1>
                <p><?php echo $page->title_sub ?></p>
            </div>

            <div class="row">

                <?php foreach ($category as $row) { ?>
                    <div class="col-md-4 col-xs-12">
                        <div class="list_home">
                            <ul>
                                <li>
                                    <a href="<?php echo $this->main->permalink(array('toko',$row->title)) ?>">
                                        <figure>
                                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                 data-src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                 alt="<?php echo $row->thumbnail_alt ?>" class="lazy">
                                        </figure>
                                        <h3><?php echo $row->title ?></h3>
                                        <br/>
                                        <div class="score"><strong><?php echo number_format($row->shop_total) ?>
                                                Toko</strong></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

</main>