<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="postcontent nobottommargin clearfix">
                <div id="posts" class="post-timeline clearfix">
                    <div class="timeline-border"></div>
                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            27<span>Jul</span>
                            <div class="timeline-divider"></div>
                        </div>
                        <div class="entry-image">
                            <a href="<?php echo $this->main->image_preview_url($page->thumbnail) ?>"
                               data-lightbox="image"><img class="image_fade"
                                                          src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>"
                                                          alt="<?php echo $page->thumbnail_alt ?>"></a>
                        </div>
                        <div class="entry-title">
                            <h1><?php echo $page->title ?></h1>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><a href="#"><i class="icon-user"></i> Admin Suka Klinik</a></li>
                        </ul>
                        <div class="entry-content">
                            <?php echo $page->description ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar nobottommargin col_last clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget clearfix">
                        <div class="tabs nobottommargin clearfix" id="sidebar-tabs">
                            <div class="tab-container">

                                <?php foreach ($promo_others as $row) {
                                    $link = $this->main->permalink(array('promo',$row->title)); ?>

                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="<?php echo $link ?>" class="nobg">
                                                <img class="rounded-circle"
                                                     src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                     alt="<?php echo $row->thumbnail_alt ?>"></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="<?php echo $link ?>"><?php echo $row->title ?></a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li><?php echo $this->main->date_view($row->created_at) ?></li>
                                            </ul>
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>