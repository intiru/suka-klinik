    <div class="section parallax nobottommargin nobottomborder"
     style="background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>');"
     data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix dark">
        <div class="heading-block center">
            <h1><?php echo $page->title ?></h1>
            <span><?php echo $page->title_sub ?></span>
        </div>
    </div>
</div>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="masonry-thumbs grid-4" data-big="4" data-lightbox="gallery">
                <?php foreach($data as $row) { ?>
                <a href="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" data-lightbox="gallery-item">
                    <img class="image_fade" src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->thumbnail_alt ?>">
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
</section>