<main class="bg_color">
    <div class="collapse" id="collapseMap">
        <div id="map" class="map"></div>
    </div>

    <div class="bg_gray">
        <div class="container margin_60_40">
            <div class="page_header">
                <div class="breadcrumbs text-center">
                    <ul>
                        <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                        <li><?php echo $page->title ?></li>
                    </ul>
                </div>
            </div>
            <div class="main_title center add_bottom_10">
                <span><em></em></span>
                <h1><?php echo $page->title ?></h1>
                <p><?php echo $page->title_sub ?></p>
            </div>
            <div class="row justify-content-md-center how_2">
                <?php echo $page->description ?>
            </div>
            <div class="text-center">

            </div>
        </div>

    </div>
</main>