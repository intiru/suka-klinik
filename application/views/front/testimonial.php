<div class="section parallax nobottommargin nobottomborder"
     style="background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>');"
     data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix dark">
        <div class="heading-block center">
            <h1><?php echo $page->title ?></h1>
            <span><?php echo $page->title_sub ?></span>
        </div>
    </div>
</div>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row">
                <ul class="testimonials-grid grid-3 clearfix">
                    <?php foreach($testimonial_list as $row) { ?>
                    <li>
                        <div class="testimonial">
                            <div class="testi-content">
                                <p><?php echo $row->description ?></p>
                                <div class="testi-meta">
                                    <?php echo $row->title ?>
                                    <span>from Google Reviews.</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>