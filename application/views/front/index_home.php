<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/template_front/img/favicon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/dark.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/font-icons.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/magnific-popup.css" type="text/css"/>

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template_front/css/responsive.css" type="text/css"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>assets/template_front/include/rs-plugin/css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>assets/template_front/include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>assets/template_front/include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/template_front/css/custom.css">
    <style>

        .revo-slider-emphasis-text {
            font-size: 64px;
            font-weight: 700;
            letter-spacing: -1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }

        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }

        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }

        .tp-video-play-button {
            display: none !important;
        }

        .tp-caption {
            white-space: nowrap;
        }

    </style>
</head>

<body class="stretched">

<div id="wrapper" class="clearfix">
    <header id="header" class="transparent-header dark" data-sticky-class="not-dark">
        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <div id="logo">
                    <a href="<?php echo site_url() ?>" class="standard-logo"
                       data-dark-logo="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"><img
                                src="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"
                                alt="Canvas Logo"></a>
                    <a href="<?php echo site_url() ?>" class="retina-logo"
                       data-dark-logo="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"><img
                                src="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png"
                                alt="Canvas Logo"></a>
                </div>

                <nav id="primary-menu" class="dark">

                    <ul>
                        <li class="current"><a href="<?php echo site_url() ?>">
                                <div>Beranda</div>
                            </a></li>
                        <li>
                            <a href="#">
                                <div>Layanan Kami</div>
                            </a>
                            <ul>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Rawat Jalan</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>UGD</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Dokter Gigi</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Home Care & Doctor on Call</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Kebidanan, Kesehatan Ibu & Anak</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Rumah Sunat ( circumcision )</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Infus Vitamine & Vaksinasi Dewasa</div>
                                    </a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <div>Profil Dokter</div>
                            </a>
                            <ul>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Dokter Frisca</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Dokter Roni</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Dokter Ary</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>DRG. Dayu Sundari</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>DRG. Prita</div>
                                    </a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <div>Promo</div>
                            </a>
                            <ul>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Promo Gigi</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Promo Sunat</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Promo Infus</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Promo Vaksin</div>
                                    </a></li>
                                <li><a href="#">
                                        <div><i class="icon-chevron-right"></i>Rapid Test Covid 19</div>
                                    </a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url('galeri-foto') ?>">
                                <div>Galeri Foto</div>
                            </a></li>
                        <li><a href="<?php echo site_url('blog') ?>">
                                <div>Blog</div>
                            </a></li>
                        <li><a href="<?php echo site_url('tentang-kami') ?>">
                                <div>Tentang Kami</div>
                            </a></li>
                        <li><a href="<?php echo site_url('kontak-kami') ?>">
                                <div>Kontak Kami</div>
                            </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <?php echo $content ?>

    <footer id="footer" class="dark">

        <div class="container"
             style="background: url('<?php echo base_url() ?>assets/template_front/images/world-map.png') no-repeat center center; background-size: 100%;">
            <div class="footer-widgets-wrap clearfix">
                <div class="col_full">
                    <div class="widget clearfix center">
                        <img src="<?php echo base_url() ?>assets/template_front/images/logo-suka-klinik.png" alt=""
                             height="100">
                        <p>Klinik Pratama Rawat Jalan , Suka Klinik dibawah PT. Suka Prima Sejahtera dengan Ijin
                            Operasional Nomor: 2411/DPMPTSP/KL.P/V/2017 yang beralamat di Jl. Tegal Permai No.88 , Perum
                            Dalung Permai, Kerobokan Kaja , Badung - Bali.
                        </p>
                        <address>
                            <strong>Alamat:</strong><br>
                            <a href="<?php echo $address_link ?>" target="_blank"><?php echo $address ?></a>
                        </address>
                        <abbr title="Phone Number">
                            <strong>HP / WhatsApp:</strong>
                        </abbr>
                        <a href="<?php echo $whatsapp_link ?>" target="_blank">
                            <?php echo $whatsapp ?>
                        </a><br>
                        <abbr title="Telepon">
                            <strong>Telepon:</strong>
                        </abbr>
                        <a href="<?php echo $phone_office_link ?>" target="_blank">
                            <?php echo $phone_office ?>
                        </a><br>
                        <abbr title="Email Address">
                            <strong>Email:</strong>
                        </abbr>
                        <a href="<?php echo $email_link ?>" target="_blank">
                            <?php echo $email ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="copyrights">
            <div class="container clearfix">
                <div class="col_one_fourth">
                    Suka Klinik © <?php echo date('Y') ?> <a href="https://www.intiru.com" target="_blank">INTIRU</a> Developed
                </div>
                <div class="col_three_fourth col_last">
                    <div class="fright clearfix">
                        <div class="copyrights-menu copyright-links nobottommargin">
                            <a href="#">Beranda</a>/
                            <a href="#">Layanan Kami</a>/
                            <a href="#">Profil Dokter</a>/
                            <a href="#">Promo</a>/
                            <a href="#">Galeri Foto</a>/
                            <a href="#">Blog</a>/
                            <a href="#">Tentang Kami</a>/
                            <a href="#">Kontak Kami</a>
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer>
</div>

<div id="gotoTop" class="icon-angle-up"></div>

<script src="<?php echo base_url() ?>assets/template_front/js/jquery.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/plugins.js"></script>

<script src="<?php echo base_url() ?>assets/template_front/js/functions.js"></script>

<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>

<script>

    var tpj = jQuery;
    tpj.noConflict();

    tpj(document).ready(function () {

        var apiRevoSlider = tpj('#rev_slider_779_1').show().revolution(
            {
                sliderType: "standard",
                jsFileLocation: "<?php echo base_url() ?>assets/template_front/include/rs-plugin/js/",
                dottedOverlay: "none",
                sliderLayout: "fullwidth",
                delay: 9000,
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1200, 1024, 778, 480],
                gridheight: [700, 768, 960, 720],
                hideThumbs: 200,

                thumbWidth: 100,
                thumbHeight: 50,
                thumbAmount: 3,

                touchenabled: "on",
                onHoverStop: "on",

                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,


                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "hades",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        }
                    },
                    thumbnails: {
                        style: "hades",
                        enable: true,
                        width: 50,
                        height: 50,
                        min_width: 100,
                        wrapper_padding: 5,
                        wrapper_color: "transparent",
                        wrapper_opacity: "1",
                        tmp: '<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
                        visibleAmount: 5,
                        hide_onmobile: false,
                        hide_onleave: false,
                        direction: "horizontal",
                        span: false,
                        position: "inner",
                        space: 5,
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 20
                    }
                },

                soloArrowLeftHalign: "left",
                soloArrowLeftValign: "center",
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: "right",
                soloArrowRightValign: "center",
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: "off",
                fullScreen: "on",

                spinner: "spinner0",

                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: "off",


                forceFullWidth: "off",
                fullScreenAlignForce: "off",

                hideThumbsOnMobile: "off",
                hideNavDelayOnMobile: 1500,
                hideBulletsOnMobile: "off",
                hideArrowsOnMobile: "off",
                hideThumbsUnderResolution: 0,

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
            });

        apiRevoSlider.bind("revolution.slide.onloaded", function (e) {
            setTimeout(function () {
                SEMICOLON.slider.sliderParallaxDimensions();
            }, 200);
        });

        apiRevoSlider.bind("revolution.slide.onchange", function (e, data) {
            SEMICOLON.slider.revolutionSliderMenu();
        });

    }); //ready

</script>

</body>
</html>