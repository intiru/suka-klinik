<section id="slider" class="slider-element slider-parallax full-screen dark error404-wrap" style="background: url(<?php echo $this->main->image_preview_url('team-suka-klinik-dalung-badung-bali.jpg') ?>) center;">
    <div class="slider-parallax-inner">
        <div class="container-fluid vertical-middle center clearfix">
            <div class="error404">404</div>
            <div class="heading-block nobottomborder">
                <h4>Ooopps.! Halaman yang kamu cari tidak ditemukan.</h4>
                <span>Tekan tombol dibawah untuk kembali ke Beranda:</span>
            </div>
            <a href="<?php echo site_url() ?>" class="btn btn-primary" type="button">Kembali ke Beranda</a>
        </div>
    </div>
</section>