<section id="page-title" class="page-title-parallax page-title-dark"
         style="padding: 120px 0; background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>'); background-size: cover; background-position: top;"
         data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px 0px;">
    <div class="container clearfix center">
        <h1><?php echo $page->title ?></h1>
        <span><?php echo $page->title_sub ?></span>
    </div>
</section>
<section id="content" class="nobottommargin">
    <div class="content-wrap nobottommargin">
        <div class="container clearfix">
            <div class="col_one_fourth">
                <div class="heading-block fancy-title nobottomborder title-bottom-border">
                    <h4><?php echo $home_siapa->title ?></h4>
                </div>
                <?php echo $home_siapa->description ?>
            </div>
            <div class="col_one_fourth">
                <div class="heading-block fancy-title nobottomborder title-bottom-border">
                    <h4><?php echo $home_moto->title ?></h4>
                </div>
                <?php echo $home_moto->description ?>
            </div>

            <div class="col_one_fourth">
                <div class="heading-block fancy-title nobottomborder title-bottom-border">
                    <h4><?php echo $home_visi->title ?></h4>
                </div>
                <?php echo $home_visi->description ?>
            </div>

            <div class="col_one_fourth col_last">
                <div class="heading-block fancy-title nobottomborder title-bottom-border">
                    <h4><?php echo $home_misi->title ?></h4>
                </div>
                <?php echo $home_misi->description ?>
            </div>
        </div>
        <div class="container clearfix border-top notopmargin col-padding">

            <div class="container clearfix notopmargin">
                <div class="col_full heading-block center">
                    <h1><?php echo $home_services_title->title ?></h1>
                    <span>
                <p><?php echo $home_services_title->title_sub ?></p>
                    </span>
                </div>
                <div class="row">
                    <?php foreach($home_services_list as $row) : ?>
                        <div class="col-12 col-sm-6 col-md-3 col-lg-3 bottommargin">
                            <div class="feature-box media-box">
                                <div class="fbox-media">
                                    <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                         alt="<?php echo $row->thumbnail_alt ?>" title="<?php echo $row->thumbnail_alt ?>">
                                </div>
                                <h3><?php echo $row->title ?></h3>
                                <?php echo $row->description ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="section layanan-unggulan">

            <div class="container nobottommargin nopadding clearfix">


                <div class="heading-block center">
                    <h2><?php echo $home_services_top_title->title ?></h2>
                    <span><?php echo $home_services_top_title->title_sub ?></span>
                </div>

                <div class="row">

                    <?php foreach($home_services_top_list as $row) { ?>

                        <div class="col-12 col-sm-6 col-md-6 bottommargin">
                            <div class="feature-box fbox-plain fbox-small fbox-dark">
                                <div class="fbox-icon">
                                    <i class="icon-line-star"></i>
                                </div>
                                <h3><?php echo $row->title ?></h3>
                                <?php echo $row->description ?>
                            </div>
                        </div>

                    <?php } ?>

                </div>

            </div>

        </div>
    </div>

</section>