<div class="section parallax nobottommargin nobottomborder"
     style="background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>');"
     data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix dark">
        <div class="heading-block center">
            <h1><?php echo $page->title ?></h1>
            <span><?php echo $page->title_sub ?></span>
        </div>
    </div>
</div>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="postcontent nobottommargin clearfix">

                <?php if(count($blog_list) == 0) { ?>
                    <h2>Blog Content belum Tersedia</h2>
                    <p>Mohon menunggu untuk update Blog / Informasi / Berita yang terkait dengan Kesehatan, Medis dan Lingkungan Hidup. ^_^</p>
                <?php } ?>

                <?php foreach ($blog_list as $row) {
                    $link = $this->main->permalink(array('blog', $row->title)); ?>
                    <div id="posts" class="small-thumbs">

                        <div class="entry clearfix">
                            <div class="entry-image">
                                <a href="<?php echo $link ?>">
                                    <img class="image_fade"
                                         src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                         alt="<?php echo $row->thumbnail_alt ?>">
                                </a>
                            </div>
                            <div class="entry-c">
                                <div class="entry-title">
                                    <h2><a href="<?php echo $link ?>"><?php echo $row->title ?></a></h2>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li>
                                        <i class="icon-calendar3"></i> <?php echo $this->main->date_view($row->created_at) ?>
                                    </li>
                                    <li><a href="#"><i class="icon-user"></i> Admin Suka Klinik</a></li>
                                    <li><i class="icon-folder-open"></i> <a
                                                href="#"><?php echo $row->category_title ?></a></li>
                                    <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                                </ul>
                                <div class="entry-content">
                                    <p><?php echo $this->main->short_desc($row->description) ?></p>
                                    <a href="<?php echo $link ?>" class="more-link">Baca Selanjutnya</a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>

                <div class="row mb-3">
                    <div class="col-12">
                        <?php echo $this->pagination->create_links() ?>
                    </div>
                </div>
            </div>
            <div class="sidebar nobottommargin col_last clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget clearfix">
                        <h4>Kategori Blog</h4>
                        <div class="tagcloud">
                            <?php foreach ($category as $row) { ?>
                                <a href="<?php echo $this->main->permalink(array('blog', $row->title)) ?>"><?php echo $row->title ?></a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="widget clearfix">
                        <div class="tabs nobottommargin clearfix" id="sidebar-tabs">
                            <ul class="tab-nav clearfix">
                                <li><a href="#tabs-1">Popular</a></li>
                                <li><a href="#tabs-2">Recent</a></li>
                            </ul>
                            <div class="tab-container">
                                <div class="tab-content clearfix" id="tabs-1">
                                    <div id="popular-post-list-sidebar">

                                        <?php foreach ($blog_popular as $row) {
                                            $link = $this->main->permalink(array('blog', $row->title)); ?>

                                            <div class="spost clearfix">
                                                <div class="entry-image">
                                                    <a href="<?php echo $link ?>" class="nobg">
                                                        <img class="rounded-circle"
                                                             src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                             alt="<?php echo $row->thumbnail_alt ?>">
                                                    </a>
                                                </div>
                                                <div class="entry-c">
                                                    <div class="entry-title">
                                                        <h4><a href="<?php echo $link ?>"><?php echo $row->title ?></a>
                                                        </h4>
                                                    </div>
                                                    <ul class="entry-meta">
                                                        <li>
                                                            <i class="icon-eye-open"></i> <?php echo number_format($row->views) ?>
                                                            Views
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="tab-content clearfix" id="tabs-2">
                                    <div id="recent-post-list-sidebar">

                                        <?php foreach ($blog_recent as $row) {
                                            $link = $this->main->permalink(array('blog', $row->title)); ?>

                                            <div class="spost clearfix">
                                                <div class="entry-image">
                                                    <a href="<?php echo $link ?>" class="nobg">
                                                        <img class="rounded-circle"
                                                             src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                             alt="<?php echo $row->thumbnail_alt ?>">
                                                    </a>
                                                </div>
                                                <div class="entry-c">
                                                    <div class="entry-title">
                                                        <h4><a href="<?php echo $link ?>"><?php echo $row->title ?></a>
                                                        </h4>
                                                    </div>
                                                    <ul class="entry-meta">
                                                        <li>
                                                            <?php echo $this->main->date_view($row->created_at) ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        <?php } ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>