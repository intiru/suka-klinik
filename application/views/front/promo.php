<div class="section parallax nobottommargin nobottomborder"
     style="background-image: url('<?php echo $this->main->image_preview_url('layanan-suka-klinik.jpg') ?>');"
     data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix dark">
        <div class="heading-block center">
            <h1><?php echo $page->title ?></h1>
            <span><?php echo $page->title_sub ?></span>
        </div>
    </div>
</div>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="postcontent promo nobottommargin clearfix">

                <?php foreach($data as $row) {
                    $link = $this->main->permalink(array('promo', $row->title)); ?>

                <div id="posts" class="post-timeline clearfix">
                    <div class="timeline-border"></div>
                    <div class="entry clearfix">
                        <div class="entry-timeline">
                            <?php echo date('d', strtotime($row->created_at)) ?><span><?php echo date('M', strtotime($row->created_at)) ?></span>
                            <div class="timeline-divider"></div>
                        </div>
                        <div class="entry-image">
                            <a href="<?php echo $link ?>">
                                <img class="image_fade" src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->thumbnail_alt ?>" title="<?php echo $row->title ?>">
                            </a>
                        </div>
                        <div class="entry-title">
                            <h2><a href="<?php echo $link ?>"><?php echo $row->title ?></a></h2>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><a href="<?php echo $link ?>"><i class="icon-user"></i> Admin Suka Klinik</a></li>
                        </ul>
                        <div class="entry-content">
                            <p><?php echo $this->main->short_desc($row->description) ?></p>

                        </div>
                    </div>
                </div>

                <?php } ?>

            </div>
        </div>
    </div>
</section>