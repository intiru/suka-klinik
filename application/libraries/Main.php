<?php

Class Main
{

    private $ci;
    private $web_name = 'Suka Klinik Dalung';
    private $web_url = 'sukaklinik.com';
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 100;

    public function __construct()
    {
        error_reporting(0);
        $this->ci =& get_instance();
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function web_url()
    {
        return $this->web_url;
    }

    function credit()
    {
        return 'development by <a href="https://www.intiru.com">INTIRU - Web Development</a>';
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
//				/unlink($this->path_images . $filename);
            }
        }
    }

    function data_main()
    {
        $id_language = $this->ci->session->userdata('id_language') ?
            $this->ci->session->userdata('id_language') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;

        $data = array(
            'web_name' => $this->web_name,
            'menu_list' => $this->menu_list(),
            'name' => $this->ci->session->userdata('name'),
            'language' => $this->ci->db->where('use', 'yes')->get('language')->result(),
            'id_language' => $id_language,
        );

        $tab_language = $this->ci->load->view('admins/components/tab_language', $data, TRUE);
        $data['tab_language'] = $tab_language;

        return $data;
    }

    function data_front()
    {

        $lang_code = 'id';
        $lang_active = $this->ci->db->where('code', $lang_code)->get('language')->row();
        $footer_desc = $this->ci->db->select('description')->where(array('type' => 'footer', 'id_language' => $lang_active->id))->get('pages')->row()->description;
//		$service_list = $this->ci->db->select('title,id')->where('id_language', $lang_active->id)->order_by('title', 'ASC')->get('category')->result();
//        $language_list = $this->ci->db->where('use', 'yes')->order_by('title', 'ASC')->get('language')->result();
//        $popup_trial = $this->ci->db->where(array('id_language' => $lang_active->id, 'type' => 'free_trial'))->get('pages')->row();
        $show_hide = TRUE;
//        $menu_list = $this->ci
//            ->db
//            ->select('title_menu, type')
//            ->where('controller_method is NOT NULL', NULL, FALSE)
//            ->where('id_language', $lang_active->id)
//            ->get('pages')
//            ->result();
//        $dictionary = $this->ci->db->where('id_language', $lang_active->id)->get('dictionary')->result();

        $category = $this->ci->db->where(array('use' => 'yes', 'id_language' => $lang_active->id))->get('category')->result();

        foreach($category as $row) {
            $row->shop_total = $this
                ->ci
                ->db
                ->where(array(
                    'id_language' => $lang_active->id,
                    'id_category' => $row->id,
                    'use' => 'yes'
                ))
                ->get('shop')
                ->num_rows();
        }

        $data = array(
            'address' => 'Perum Dalung Permai, Jl. Tegal Permai No.88, Kerobokan Kaja, Kec. Kuta Utara, Kabupaten Badung, Bali 80361',
            'address_short' => 'Dalung, Kerobokan Kaja, Kuta Utara, Badung, Bali',
            'address_link' => 'https://g.page/sukaklinik?share',
            'telephone' => '0814 123 123 123',
            'phone_hp' => '+62878 6166 1920',
            'phone_hp_link' => 'tel:+6287861661920',
            'phone_office' => '+62361419129',
            'phone_office_link' => 'tel:+62361419129',
            'whatsapp' => '+62878 6166 1920',
            'whatsapp_link' => 'https://api.whatsapp.com/send?phone=6287861661920&text=Hello,%20i%20want%20to%20ask%20you%20about%20your%20services.',
            'wechat_id' => '',
            'wechat_link' => '',
            'email_link' => 'mailto:sukaclinicbali@gmail.com',
            'email' => 'sukaclinicbali@gmail.com',
            'facebook_link' => 'https://www.facebook.com/sukaklinik',
            'line_link' => 'https://msng.link/ln/harsa.bali.holiday',
            'twitter_link' => 'https://twitter.com/max99financial',
            'linkedin_link' => '',
            'instagram_link' => 'https://www.instagram.com/sukaklinikdalung/',
            'view_secret' => FALSE,
            'author' => 'www.sukaklinik.com',
//			'services_list' => $service_list,
//            'language_list' => $language_list,
            'lang_code' => $lang_code,
            'lang_active' => $lang_active,
            'id_language' => $lang_active->id,
            'footer_desc' => $footer_desc,
//            'popup_trial' => $popup_trial,
//            'captcha' => $this->captcha(),
            'show_hide' => $show_hide,
            'category' => $category
        );

//        foreach ($menu_list as $row) {
//            $data[$row->type] = $row;
//        }
//
//        foreach($dictionary as $row) {
//            $data['dict_'.$row->dict_variable] = $row->dict_word;
//        }

//        echo json_encode($data);
//        exit;

        return $data;
    }

    function translate_number($number)
    {
        switch ($number) {
            case 1:
                return "first";
                break;
            case 2:
                return "second";
                break;
            case 3:
                return "third";
                break;
            case 4:
                return "four";
                break;
        }
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('login');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            redirect('intiru/dashboard');
        }
    }

    function permalink($data=array())
    {

        $slug = '';
        foreach ($data as $r) {
            $slug .= $this->slug($r) . '/';
        }

        return site_url($slug);
    }

    function breadcrumb($data)
    {
        $breadcrumb = '<ul class="breadcrumb">';
        $count = count($data);
        $no = 1;
        foreach ($data as $url => $label) {
            $current = '';
            if ($no == $count) {
                $current = ' class="current"';
            }

            $breadcrumb .= '<li' . $current . '><a href="' . $url . '">' . $label . '</a></li>';
        }

        $breadcrumb .= '</ul>';


        return $breadcrumb;
    }

    function slug($text)
    {

        $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '?','!');
        $replace = array('-', '-', 'and', '-', '-', '-', '', '', '','');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function slug_back($slug)
    {
        $slug = trim($slug);
        if (empty($slug)) return '';
        $slug = str_replace('-', ' ', $slug);
        $slug = ucwords($slug);
        return $slug;
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function upload_file_slider($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 600;
        $config['max_width'] = 2000;
        $config['max_height'] = 1400;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function captcha()
    {
        $this->ci->load->helper(array('captcha', 'string'));
        $this->ci->load->library('session');

        $vals = array(
            'img_path' => './upload/images/captcha/',
            'img_url' => base_url() . 'upload/images/captcha',
            'img_width' => '200',
            'img_height' => 35,
            'border' => 0,
            'expiration' => 7200,
            'word' => random_string('numeric', 5)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $captcha = $cap['image'];

        // store the captcha word in a session
        //$cap['word'];
        $this->ci->session->set_userdata('captcha_mwz', $cap['word']);

        return $captcha;
    }

    function share_link($socmed_type, $title, $link)
    {
        switch ($socmed_type) {
            case "facebook":
                return "https://www.facebook.com/sharer/sharer.php?u=" . $link;
                break;
            case "twitter":
                return "https://twitter.com/home?status=" . $link;
                break;
            case "googleplus":
                return "https://plus.google.com/share?url=" . $link;
                break;
            case "linkedin":
                return "https://www.linkedin.com/shareArticle?mini=true&url=" . $link . "&title=" . $title . "&summary=&source=";
                break;
            case "pinterest":
                return "https://pinterest.com/pin/create/button/?url=" . $title . "&media=" . $link . "&description=";
                break;
            case "email":
                return "mailto:" . $link . "?&subject=" . $title;
            default:
                return $link;
                break;
        }
    }

    function mailer_auth($subject, $to_email, $to_name, $body, $file = '')
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "mail.sukaklinik.com"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "no-reply@sukaklinik.com"; //user email
            $mail->Password = "NoReply123!@#"; //password email
            $mail->SetFrom("no-reply@sukaklinik.com ", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            if ($file) {
                $mail->addAttachment("upload/images/" . $file);
            }
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    function menu_list()
    {
        $menu = array(
            'MAIN' => array(
                'dashboard' => array(
                    'label' => 'Dashboard',
                    'route' => base_url('intiru/dashboard'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'view_front' => array(
                    'label' => 'View Website',
                    'route' => base_url(''),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                )
            ),
            'PAGES' => array(
                'home' => array(
                    'label' => 'Beranda',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halama Umum Beranda',
                            'route' => base_url('intiru/pages/type/home'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'home_slider' => array(
                            'label' => 'Home Slider',
                            'route' => base_url('intiru/home_slider'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_title' => array(
                            'label' => 'Judul Jenis Layanan',
                            'route' => base_url('intiru/pages/type/home_jenis_layanan'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_data' => array(
                            'label' => 'Daftar Jenis Layanan',
                            'route' => base_url('intiru/home_services'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_top_title' => array(
                            'label' => 'Judul Layanan Unggulan',
                            'route' => base_url('intiru/pages/type/home_layanan_unggulan'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_top_data' => array(
                            'label' => 'Daftar Layanan Unggulan',
                            'route' => base_url('intiru/home_services_top'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'moto' => array(
                            'label' => 'Penjelasan Moto Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_moto'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'siapa' => array(
                            'label' => 'Penjelasan Siapa Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_siapa'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'visi' => array(
                            'label' => 'Penjelasan Visi Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_visi'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'misi' => array(
                            'label' => 'Penjelasan Misi Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_misi'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'services' => array(
                    'label' => 'Layanan Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'ebook_page' => array(
                            'label' => 'Halaman Layanan Kami',
                            'route' => base_url('intiru/pages/type/services'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_list' => array(
                            'label' => 'Daftar Layanan',
                            'route' => base_url('intiru/services'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'slogan' => array(
                            'label' => 'Kutipan',
                            'route' => base_url('intiru/pages/type/services_quotes'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'doctor' => array(
                    'label' => 'Profil Dokter',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'ebook_page' => array(
                            'label' => 'Halaman Profil Dokter',
                            'route' => base_url('intiru/pages/type/doctor'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'doctor_list' => array(
                            'label' => 'Daftar Dokter',
                            'route' => base_url('intiru/doctor'),
                            'icon' => 'fab fa-asymmetrik'
                        )
                    )
                ),
                'promo' => array(
                    'label' => 'Promo',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'promo_page' => array(
                            'label' => 'Halaman Promo',
                            'route' => base_url('intiru/pages/type/promo'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'promo_list' => array(
                            'label' => 'Data Promo',
                            'route' => base_url('intiru/promo'),
                            'icon' => 'fab fa-asymmetrik'
                        )
                    )
                ),
                'gallery_photo' => array(
                    'label' => 'Galeri Foto',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'gallery_photo_page' => array(
                            'label' => 'Halaman Galeri Foto',
                            'route' => base_url('intiru/pages/type/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'gallery_photo_list' => array(
                            'label' => 'Daftar Galeri Foto',
                            'route' => base_url('intiru/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                    )
                ),
                'blog' => array(
                    'label' => 'Blog',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'blog_page' => array(
                            'label' => 'Halaman Blog',
                            'route' => base_url('intiru/pages/type/blog'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_category' => array(
                            'label' => 'Kategori Blog',
                            'route' => base_url('intiru/blog_category'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_list' => array(
                            'label' => 'Daftar Blog',
                            'route' => base_url('intiru/blog_content'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'testimonial' => array(
                    'label' => 'Testimonial',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'testimonial_page' => array(
                            'label' => 'Testimonial Page',
                            'route' => base_url('intiru/pages/type/testimonial'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'testimonial_list' => array(
                            'label' => 'Testimonial List',
                            'route' => base_url('intiru/testimonial'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),

				'about_us' => array(
					'label' => 'Tentang Kami',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
					    'about_page' => array(
                            'label' => 'Halaman Tentang Kami',
                            'route' => base_url('intiru/pages/type/about_us'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_title' => array(
                            'label' => 'Judul Jenis Layanan',
                            'route' => base_url('intiru/pages/type/home_jenis_layanan'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_data' => array(
                            'label' => 'Daftar Jenis Layanan',
                            'route' => base_url('intiru/home_services'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_top_title' => array(
                            'label' => 'Judul Layanan Unggulan',
                            'route' => base_url('intiru/pages/type/home_layanan_unggulan'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services_top_data' => array(
                            'label' => 'Daftar Layanan Unggulan',
                            'route' => base_url('intiru/home_services_top'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'moto' => array(
                            'label' => 'Penjelasan Moto Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_moto'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'siapa' => array(
                            'label' => 'Penjelasan Siapa Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_siapa'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'visi' => array(
                            'label' => 'Penjelasan Visi Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_visi'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'misi' => array(
                            'label' => 'Penjelasan Misi Suka Klinik',
                            'route' => base_url('intiru/pages/type/home_misi'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
				),

                'contact_us' => array(
                    'label' => 'Kontak Kami',
                    'route' => base_url('intiru/pages/type/contact_us'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),

//                'category' => array(
//                    'label' => 'Kategori Toko',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'ebook_page' => array(
//                            'label' => 'Halaman Kategori Toko',
//                            'route' => base_url('intiru/pages/type/category'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'category' => array(
//                            'label' => 'Data Kategori Toko',
//                            'route' => base_url('intiru/category'),
//                            'icon' => 'fab fa-asymmetrik'
//                        )
//                    )
//                ),
//                'shop' => array(
//                    'label' => 'Daftar Toko',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//						'shop_page' => array(
//							'label' => 'Halaman List Toko',
//							'route' => base_url('intiru/pages/type/list_shop'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//                        'list_shop' => array(
//                            'label' => 'Data List Toko',
//                            'route' => base_url('intiru/shop'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    )
//                ),
//				'location' => array(
//					'label' => 'Lokasi',
//					'route' => base_url('intiru/pages/type/location'),
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array()
//				),
//				'report' => array(
//					'label' => 'Pengaduan',
//					'route' => base_url('intiru/pages/type/report'),
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array()
//				),
//                'free_trial' => array(
//                    'label' => 'Free Trial',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'free_trial_page' => array(
//                            'label' => 'Popup Free Trial',
//                            'route' => base_url('intiru/pages/type/free_trial'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'free_trial_list' => array(
//                            'label' => 'Data Request Free Trial',
//                            'route' => base_url('intiru/free_trial_request'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    )
//                ),
//
//                'profile' => array(
//                    'label' => 'Tentang Kami',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'profile_page' => array(
//                            'label' => 'Halaman Tentang Kami',
//                            'route' => base_url('intiru/pages/type/about_us'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
////                        'profile_team' => array(
////                            'label' => 'Daftar Team',
////                            'route' => base_url('intiru/profile_team'),
////                            'icon' => 'fab fa-asymmetrik'
////                        ),
//                    )
//                ),
//				'gallery_video' => array(
//					'label' => 'Gallery Video',
//					'route' => 'javascript:;',
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array(
//						'gallery_photo_page' => array(
//							'label' => 'Gallery Video Page',
//							'route' => base_url('intiru/pages/type/gallery_video'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//						'gallery_photo_list' => array(
//							'label' => 'Gallery Video List',
//							'route' => base_url('intiru/gallery_video'),
//							'icon' => 'fab fa-asymmetrik'
//
//						),
//					)
//				),


//                'price_list' => array(
//                    'label' => 'Daftar Harga',
//                    'route' => base_url('intiru/pages/type/price_list'),
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array()
//                ),

//                'faq' => array(
//                    'label' => 'FAQ',
//                    'route' => base_url('intiru/pages/type/faq'),
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array()
//                ),
//

//                'reservation' => array(
//                    'label' => 'Reservation',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'reservation_page' => array(
//                            'label' => 'Reservation Page',
//                            'route' => base_url('intiru/pages/type/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'reservation_list' => array(
//                            'label' => 'Reservation List',
//                            'route' => base_url('intiru/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    )
//                ),
                'footer' => array(
                    'label' => 'Footer Web',
                    'route' => base_url('intiru/pages/type/footer'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
//                'dictionary' => array(
//                    'label' => 'Kamus Web',
//                    'route' => base_url('intiru/dictionary'),
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array()
//                ),
            ),
            'OTHERS MENU' => array(
                'file_manager' => array(
                    'label' => 'File Manager',
                    'route' => base_url('intiru/file_manager'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'email' => array(
                    'label' => 'Email',
                    'route' => base_url('intiru/email'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
//                'language' => array(
//                    'label' => 'Language',
//                    'route' => base_url('intiru/language'),
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array()
//                ),
                'admin' => array(
                    'label' => 'Manage Admin',
                    'route' => base_url('intiru/admin'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }
}
