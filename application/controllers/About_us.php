<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'about_us', 'id_language' => $data['id_language']))->get('pages')->row();

        $data['home_siapa'] = $this
            ->db
            ->select('title, description')
            ->where('type', 'home_siapa')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();

        $data['home_moto'] = $this
            ->db
            ->select('title, description')
            ->where('type', 'home_moto')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();
        $data['home_visi'] = $this
            ->db
            ->select('title, description')
            ->where('type', 'home_visi')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();
        $data['home_misi'] = $this
            ->db
            ->select('title, description')
            ->where('type', 'home_misi')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();

        $data['home_services_title'] = $this
            ->db
            ->select('title, title_sub')
            ->where('type', 'home_jenis_layanan')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();
        $data['home_services_list'] = $this
            ->db
            ->where('use', 'yes')
            ->where('id_language', $data['id_language'])
            ->order_by('title', 'ASC')
            ->get('home_services')
            ->result();
        $data['home_services_top_title'] = $this
            ->db
            ->select('title, title_sub')
            ->where('type', 'home_layanan_unggulan')
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->row();
        $data['home_services_top_list'] = $this
            ->db
            ->where('use', 'yes')
            ->where('id_language', $data['id_language'])
            ->order_by('title', 'ASC')
            ->get('home_services_top')
            ->result();

        $this->template->front('about_us', $data);
    }
}
