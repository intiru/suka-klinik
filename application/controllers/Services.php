<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'services', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['services_list'] = $this
            ->db
            ->where(array(
                'id_language' => $data['id_language'],
                'use' => 'yes'
            ))
            ->order_by('title', 'ASC')
            ->get('services')
            ->result();
        $data['services_quotes'] = $this->db->select('title')->where(array('type' => 'services_quotes', 'id_language' => $data['id_language']))->get('pages')->row()->title;
        $data['testimonial_title'] = $this
            ->db
            ->select('title')
            ->where(array(
                'id_language' => $data['id_language'],
                'type' => 'testimonial'
            ))
            ->get('pages')
            ->row()
            ->title;
        $data['testimonial_list'] = $this
            ->db
            ->select('title, description')
            ->where(array(
                'use' => 'yes'
            ))
            ->order_by('id', 'DESC')
            ->get('comment', 15, 0)
            ->result();

        $this->template->front('services', $data);
    }
}
