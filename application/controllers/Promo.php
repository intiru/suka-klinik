<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'promo', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['data'] = $this
            ->db
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language']
            ))
            ->order_by('id', 'DESC')
            ->get('promo')
            ->result();

        $this->template->front('promo', $data);
    }

    public function detail($id)
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where('id', $id)->get('promo')->row();
        $data['page']->type = 'promo';
        $data['promo_others'] = $this
            ->db
            ->where(array(
                'id_language' => $data['id_language']
            ))
            ->where_not_in('id', array($id))
            ->order_by('id', 'DESC')
            ->get('promo')
            ->result();

        $this->template->front('promo_detail', $data);
    }
}
