<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'doctor', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['doctor_list'] = $this->db->where(array('use'=>'yes', 'id_language' => $data['id_language']))->order_by('title', 'ASC')->get('doctor')->result();

        $this->template->front('doctor', $data);
    }
}
