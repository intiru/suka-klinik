<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('m_doctor');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {
        $data = $this->main->data_main();
        $data['category'] = $this->m_doctor->get_data()->result();
        $this->template->set('category', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Management Category');
        $this->template->load_admin('doctor/index', $data);
    }

    public function createprocess()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alt', 'required');

        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                    'description' => form_error('description'),
                    'thumbnail_alt' => form_error('thumbnail_alt'),
                )
            ));
        } else {

            $data = $this->input->post(NULL);
            $data['id_language'] = $this->session->userdata('id_language');

            if ($_FILES['thumbnail']['name']) {
                $response = $this->main->upload_file_thumbnail('thumbnail', $this->input->post('thumbnail_filename'));
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'thumbnail' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['thumbnail'] = $response['filename'];
                }
            }

            $this->m_doctor->input_data($data);

            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
            ));
        }
    }

    public function delete($id)
    {
        $where = array('id' => $id);
//		$row = $this->m_blog_category->row_data($where);
//		$this->main->delete_file($row->thumbnail);
        $this->m_doctor->delete_data($where);
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alt', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                    'thumbnail_alt' => form_error('thumbnail_alt'),
                )
            ));
        } else {

            $id = $this->input->post('id');
            $data = $this->input->post(NULL);
            $where = array(
                'id' => $id
            );

            if ($_FILES['thumbnail']['name']) {
                $response = $this->main->upload_file_thumbnail('thumbnail', $this->input->post('thumbnail_filename'));
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'thumbnail' => $response['message']
                        )
                    ));
                    exit;
                } else {
//					$row_data = $this->m_blog_category->row_data($where);
//					$this->main->delete_file($row_data->thumbnail);

                    $data['thumbnail'] = $response['filename'];
                }
            } else {
                $full_path = BASEPATH."../";
                $thumbnail_filename_before = $this->db->where('id', $id)->get('doctor')->row()->thumbnail;
                $thumbnail_file_array = $array = explode('.', $thumbnail_filename_before);
                $thumbnail_file_extension = end($thumbnail_file_array);
                $thumbnail_filename_now = $this->main->slug($this->input->post('thumbnail_filename')) . '.' . $thumbnail_file_extension;
                rename("$full_path/upload/images/$thumbnail_filename_before", "$full_path/upload/images/$thumbnail_filename_now");

                $data['thumbnail'] = $thumbnail_filename_now;
            }


            $this->m_doctor->update_data($where, $data);
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
                'thumbnail_filename_now' => $thumbnail_filename_now,
                'thumbnail_filename_before' => $thumbnail_filename_before,
                'ext' => $thumbnail_file_extension,
                'fullpath' => $full_path,
                'id' => $id
            ));
        }
    }
}
