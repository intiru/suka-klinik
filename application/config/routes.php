<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$connection = mysqli_connect('localhost', 'root', 'root', 'intiru_sukaklinik');

//$lang_code_list = array('id','jp','en','fr','zh');
$lang_code_list = array();


$route['default_controller'] = 'home';
$route['intiru'] = 'intiru/login';
$route['intiru/login'] = 'intiru/login/process';

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;
$code = '';
//foreach($lang_code_list as $code) {

    $route[$code.'layanan'] = 'services';
    $route[$code.'profil-dokter'] = 'doctor';
    $route[$code.'promo'] = 'promo';
    $route[$code.'promo/detail'] = 'promo/detail';
    $route[$code.'galeri-foto'] = 'gallery';
    $route[$code.'blog'] = 'blog';
    $route[$code.'blog/kategori'] = 'blog';
    $route[$code.'blog/detail'] = 'blog/detail/1';
    $route[$code.'blog/(:num)'] = 'blog';
    $route[$code.'tentang-kami'] = 'about_us';
    $route[$code.'kontak-kami'] = 'contact_us';
    $route[$code.'kontak-kami-send'] = 'contact_us/send';

    $route['^'.$code.'/(.+)$'] = "$1";
    $route['^'.$code.'$'] = $route['default_controller'];
//}

$promo = mysqli_query($connection, "SELECT id, title FROM promo");
while ($data = mysqli_fetch_array($promo)) {
    $route['promo/' . slug($data['title'])] = "promo/detail/" . $data['id'];
}
$blog_category = mysqli_query($connection, "SELECT id, title FROM blog_category");
while ($data = mysqli_fetch_array($blog_category)) {
    $route['blog/' . slug($data['title'])] = "blog/category/" . $data['id'];
    $route['blog/' . slug($data['title']).'/(:num)'] = "blog/category/" . $data['id'].'/$1';
}
$blog = mysqli_query($connection, "SELECT id, title FROM blog");
while ($data = mysqli_fetch_array($blog)) {
    $route['blog/' . slug($data['title'])] = "blog/detail/" . $data['id'];
}

function slug($string)
{
    $find = array(' ', '/', '&', '\\', '\'', ',','(',')','!');
    $replace = array('-', '-', 'and', '-', '-', '-','','','');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
